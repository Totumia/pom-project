package data;

public enum ProductData {
  Backpack("Sauce Labs Backpack"),
  BikeLight("Sauce Labs Bike Light"),
  BoltTShirt("Sauce Labs Bolt T-Shirt"),
  FleeceJacket("Sauce Labs Fleece Jacket"),
  Onesie("Sauce Labs Onesie"),
  TShirtRed("Test.allTheThings() T-Shirt (Red)");
  private String name;

  ProductData(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

}
