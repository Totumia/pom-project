package pages;

import data.ProductData;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import org.testng.Assert;

import java.util.List;


public class ProductPage extends AbsBasePage {
  public ProductPage(WebDriver driver) {
    super(driver);
  }

  @FindBy(css = ".inventory_list>div>.inventory_item_description>div>a")
  private List<WebElement> listOfProducts;
  @FindBy(css = ".inventory_details_name")
  private WebElement nameOfProduct;


  public void chooseProductByName(ProductData data) {
    for (WebElement product : listOfProducts) {
      if (product.getText().equalsIgnoreCase(data.getName())) {
        product.click();
        Assert.assertEquals(data.getName(), nameOfProduct.getText());
        break;
      }
    }
  }
}
