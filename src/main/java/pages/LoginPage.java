package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbsBasePage {

  public LoginPage(WebDriver driver) {
    super(driver);
  }


  @FindBy(css = "#user-name")
  private WebElement username;
  @FindBy(css = "#password")
  private WebElement password;
  @FindBy(css = "#login-button")
  private WebElement loginBtn;

  public ProductPage loginToSauce(){
    username.sendKeys("standard_user");
    password.sendKeys("secret_sauce");
    loginBtn.click();
    return new ProductPage(driver);
  }

}
