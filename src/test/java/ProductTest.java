import data.ProductData;
import driver.DriverFactory;
import driver.exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPage;

public class ProductTest {
  private WebDriver driver;

  @BeforeMethod
  public void init() throws DriverNotSupportedException {
    this.driver = new DriverFactory().getDriver();
  }

  @Test
  public void checkChooseProduct() {
    new LoginPage(driver)
        .open("");
    new LoginPage(driver)
        .loginToSauce()
        .chooseProductByName(ProductData.BikeLight);

  }

  @AfterMethod
  public void close() {
    if (this.driver != null) {
      this.driver.close();
      this.driver.quit();
    }
  }
}
